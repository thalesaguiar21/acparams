# Acoustic Analysis

This project implements a pipeline for a statistical analysis of speech
parameters. More specifically, for accent analysis we provide the main
differences on phones according to the literature in phonetics and linguistics.


# To Do
- [X] Phone duration
- [ ] Phone pitch
- [ ] Phone f0
