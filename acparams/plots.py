from typing import List
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import librosa
from librosa import display

OUTPUTPATH = "imgs/"
CMAP = "Greys"
EXT = "pdf"
plt.style.use("imgs/thesis.mplstyle")


def spectrogram(s: np.ndarray, fname: str, fs: int = 16000):
    """Create and save a spectrogram  in PDF of the given signal

    Args:
        s: the signal fname: the filename without extension
    """
    # freqs, times, spec = signal.spectrogram(s, fs=44100, nperseg=96,
    #                                         noverlap=86, nfft=256,
    #                                         window='hann', mode='magnitude',
    #                                         scaling='spectrum')
    # normalizer = matplotlib.colors.Normalize()
    # plt.imshow(spec, origin='lower', aspect='auto', extent=(0, times.max(), 0,
    #           freqs.max()), cmap=CMAP, norm=normalizer)
    plt.specgram(s, Fs=fs, noverlap=860, NFFT=1024)
    plt.ylabel("Frequency (Hz)")
    plt.xlabel("Time (s)")
    _tight_save_close(fname)


def f0(s: np.ndarray, srate: int, fname: str):
    """Plot the fundamental frequency of a speaker

    Args:
        s (str): the signal
        srate (int): the sampling rate of 's'
    """
    D = librosa.amplitude_to_db(np.abs(librosa.stft(s)), ref=np.max)  # type: ignore
    fig, ax = plt.subplots(figsize=(10, 4))
    img = display.specshow(D, x_axis="time", y_axis="log", ax=ax)
    f0, *_ = librosa.pyin(s, fmin=65, fmax=2100, sr=srate)
    times = librosa.times_like(f0)
    fig.colorbar(img, ax=ax, format="%+2.f dB")
    ax.plot(times, f0, label=r"$f_0$", color="cyan", linewidth=3)
    ax.legend(loc="upper right")
    _tight_save_close(fname)


def multi_spectrogram(waves: List[np.ndarray], fname: str, fs: int = 16000):
    """Create and save a spectrogram  in PDF of the given signal
    Args:
        s: the signal
        fname: the filename without extension
    """
    _, axs = plt.subplots(3, 3, sharey=True, figsize=(14, 12))
    for rowax, waverow in zip(axs, waves):
        for ax, wave in zip(rowax, waverow):
            ax.specgram(wave, Fs=fs, noverlap=86, NFFT=128)

    ylabels = [f"{accent}\n(Hz)" for accent in ["Carioca", "Mineiro", "Nordestino"]]
    for ax, label in zip(axs[:, 0], ylabels):
        ax.set_ylabel(label)

    for ax in axs[-1, :]:
        ax.set_xlabel("Time (s)")

    phonemes = ["/r/", "/s/", "/t/"]
    for ax, phoneme in zip(axs[0, :], phonemes):
        ax.set_title(phoneme, fontsize=25, fontweight="bold")

    _tight_save_close(fname)


def hbar_errors(
    scores: np.ndarray,
    err: np.ndarray,
    fname: str,
    ylabels=None,
    er_label: str = "Error",
    colors=None,
):
    if not ylabels:
        ylabels = []
    if not colors:
        colors = ["#aeadf2ff"] * (scores.size - 1) + ["#6867AC"]

    with matplotlib.rc_context({"figure.figsize": (3, 4)}):
        plt.grid(axis="x", alpha=0.7, zorder=0)
        ypos = np.arange(scores.size)
        plt.barh(
            ypos,
            scores,
            xerr=err,
            tick_label=ylabels[::-1],
            color=colors,
            zorder=3,
        )
        plt.xlabel(f"{er_label} (\\%)")
        _tight_save_close(fname)


def bar(fname: str, ylabel: str = "", bar_labels: bool = True, **bargs):
    """A customised function to plot bars with the same style across the
    document"""
    pos = np.arange(bargs["height"].size)
    plt.grid(axis="y", alpha=0.7, zorder=0)
    bars = plt.bar(pos, zorder=3, **bargs)
    plt.ylabel(ylabel)
    plt.ylim(0, 100)
    if bar_labels:
        plt.bar_label(bars, padding=2)
    _tight_save_close(fname)


def hbar(
    fname: str, xlabel: str = "", bar_labels: bool = True, no_bg: bool = False, **bargs
):
    """A customised function to plot bars with the same style across the
    document"""
    pos = np.arange(bargs["width"].size)
    with matplotlib.rc_context({"figure.figsize": (7, 2)}):
        plt.grid(axis="x", alpha=0.7, zorder=0)
        bars = plt.barh(pos, zorder=3, **bargs)
        plt.xlabel(xlabel)
        plt.xlim(0, 100)
        if bar_labels:
            plt.bar_label(bars, padding=2, fontsize="x-large")
        _tight_save_close(fname, no_bg)


def boxplots(
    fname: str,
    ylabel: str,
    ylim: list[int],
    labels,
    *values,
    colors=None,
):
    """Plot boxplots from the given data"""
    literature_mos = values
    bplots = plt.boxplot(
        literature_mos,
        notch=False,
        vert=1,
        patch_artist=True,
    )
    if not colors:
        colors = ["lightpink", "lightblue", "palegreen", "moccasin"]
    for bplot, color in zip(bplots["boxes"], colors):
        bplot.set(linewidth=2)
        bplot.set(facecolor=color)
    for whisker in bplots["whiskers"]:
        whisker.set(linewidth=2)
    for cap in bplots["caps"]:
        cap.set(linewidth=2)
    for median in bplots["medians"]:
        median.set(color="gray", linewidth=2)

    plt.ylabel(ylabel)
    plt.ylim(ylim)
    plt.xticks(np.arange(1, len(labels) + 1), labels)
    plt.grid(axis="y", alpha=0.7, zorder=0)
    _tight_save_close(fname, transp=True)


def pie(fname: str, **pieargs):
    plt.pie(autopct=lambda pct: _pie_autopct(pct, pieargs["x"]), **pieargs)
    _tight_save_close(fname, transp=True)


def _pie_autopct(pct, data):
    count = int(pct / 100.0 * sum(data))
    return f"{pct:2.1f}% \n({count})"


def _tight_save_close(fname: str, transp=False):
    plt.tight_layout()
    plt.savefig(f"{OUTPUTPATH}{fname}.{EXT}", dpi=600, transparent=transp)
    plt.close()
