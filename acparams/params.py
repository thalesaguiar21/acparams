import os
import pathlib

import numpy as np
import pandas as pd
import jiwer
from scipy import stats

_BASEPATH = "~/Dev/datasets/ttbacc"


def describe(data: list) -> tuple:
    mean = np.mean(data)
    std = np.std(data)
    mode, counts = stats.mode(data)
    return mean, std, mode, counts


def show_metrics(measures: list):
    print(f"MAX =\t{np.max(measures):.2f}%")
    print(f"MIN =\t{np.min(measures):.2f}%")
    print(f"MEAN =\t{np.mean(measures):.2f}%")
    print(f"STD =\t{np.std(measures):.2f}%")
    print(f"MED =\t{np.median(measures):.2f}%")


def compute_metrics(
    transformation: jiwer.Compose, basename: pd.Series, root: str = _BASEPATH
):
    measures = {"wer": [], "mer": [], "wil": []}
    no_hypo = []
    for fname in basename:
        path = f"{root}/wav/{fname}"
        gold_path = pathlib.PosixPath(f"{path}.hpt.txt").expanduser()
        hypo_path = pathlib.PosixPath(f"{path}.apt.txt").expanduser()
        if os.path.isfile(hypo_path):
            with open(gold_path) as gold, open(hypo_path) as hypo:
                meas = jiwer.compute_measures(
                    gold.read(),
                    hypo.read(),
                    truth_transform=transformation,
                    hypothesis_transform=transformation,
                )
                for key, value in meas.items():
                    if key in measures:
                        measures[key].append(value * 100)
        else:
            no_hypo.append(hypo_path)
    return measures, no_hypo
