IPA2sampa = {
    "a": "a",
    "b": "b",
    "ɓ": "b_<",
    "c": "c",
    "d": "d",
    "ɗ": "d`",
    "e": "e",
    "f": "f",
    "g": "g",
    "ɠ": "g_<",
    "h": "h",
    "ɦ": "h\\",
    "i": "i",
    "ʝ": "j\\",
    "j": "j",
    "k": "k",
    "l": "l",
    "ɺ": "l\\",
    "m": "m",
    "n": "n",
    "ɳ": "n`",
    "o": "o",
    "p": "p",
    "ɸ": "p\\",
    "q": "q",
    "r": "r",
    "ɽ": "r`",
    "ɹ": "r\\",
    "ɻ": "r\\`",
    "s": "s",
    "ʂ": "s`",
    "ɕ": "s\\",
    "t": "t",
    "ʈ": "t`",
    "u": "u",
    "v": "v",
    "ʋ": "v\\",
    "w": "w",
    "x": "x",
    "ɧ": "x\\",
    "y": "y",
    "z": "z",
    "ʐ": "z`",
    "ʑ": "z\\",

    # Capital
    "ɑ": "A",
    "β": "B",
    "ʙ": "B\\",
    "ç": "C",
    "ð": "D",
    "ɛ": "E",
    "ɱ ": "F",
    "ɣ": "G",
    "ɢ": "G\\",
    "ʛ": "G\\_<",
    "ɥ": "H",
    "ɪ": "I",
    "ᵻ": "I\\",
    "ɲ": "J",
    "ɟ": "J\\",
    "ʄ": "J\\_<",
    "ɬ": "K",
    "ɮ": "K\\",
    "ʎ": "L",
    "ʟ": "L\\",
    "ɯ": "M",
    "ɰ": "M\\",
    "ŋ": "N",
    "ɴ": "N\\",
    "ɔ": "O",
    "ʘ": "O\\",
    "ʋ": "P",
    "ɒ": "Q",
    "ʁ": "R",
    "ʀ": "R\\",
    "ʃ": "S",
    "θ": "T",
    "ʊ": "U",
    "ᵿ": "U\\",
    "ʌ": "V",
    "ʍ": "W",
    "χ": "X",
    "ħ": "X\\",
    "ʏ": "Y",
    "ʒ": "Z",

    # Other
    "ə": "@",
    "ɘ": "@\\",
    "ɚ": "@`",
    "æ": "{",
    "ʉ": "}",
    "ɨ": "1",
    "ø": "2",
    "ɜ": "3",
    "ɞ": "3\\",
    "ɾ": "4",
    "ɫ": "5",
    "ɐ": "6",
    "ɤ": "7",

    # Diacrits
    "j̃": "j~",
    "ĩ": "i~",
    "ũ": "u~",
    "ẽ": "e~",
    "ð": "d_<",
    "õ": "o~",
    "w̃": "w~",
    "ɐ̃": "6~",
    "ɡ": "g_<",
}
