from collections import defaultdict
import csv

import librosa
import numpy as np
import pandas as pd
from scipy.io import wavfile

import acparams.convert
import acparams.textgrid
import acparams.audio
import acparams.extractors.base

ACPATH = acparams.extractors.base.DATAPATH / "ac"
OUTPUTFILE = ACPATH / "ttbacc-ac.csv"

TARGETSPEAKER = "ted-talk-0634"
NONPHONE = ["spn", "", "sil"]


def unicode_phone(phon: str, unicodes: list) -> bool:
    return phon.encode("unicode_escape") in unicodes


def split_phones(df: pd.DataFrame, wavdir: str, queue):
    qtd_no_align = 0
    for _, row in df.iterrows():
        tgrid = acparams.extractors.base.try_to_parse(row)
        if not tgrid:
            qtd_no_align += 1
        else:
            wavpath = f"{wavdir}/{row['speaker']}/{row['fname']}.wav"
            wav, rate = librosa.load(wavpath, sr=None)  # type: ignore
            voiced_intvs = [phn for phn in tgrid.phones if phn.name not in NONPHONE]

            phones = acparams.audio.split(voiced_intvs, wav, rate, offset=160)
            for p, intv in zip(phones, tgrid.phones):
                if intv.name in acparams.convert.IPA2sampa.keys():
                    queue.put((p, row["fname"], acparams.convert.IPA2sampa[intv.name]))


def listener(q, outputpath):
    fnum = defaultdict(int)

    phones_csv = open(outputpath, "w", newline="")
    phones_writer = csv.writer(phones_csv, dialect="unix")
    csv_cols = ["fname", "phone", "speaker", "st"]
    phones_writer.writerow(csv_cols)

    while True:
        m = q.get()
        if m == "kill":
            break
        phone_data, fname, label = m
        fnum[fname] += 1
        fullname = f"{fname}-{fnum[fname]}-{label}.wav"
        sounds_path = get_folder(fname)
        wavfile.write(
            filename=sounds_path / fullname,
            rate=16000,
            data=phone_data.astype(np.int16),
        )
        st = "T" if "ted-talk-0634" in fname else "S"
        phones_writer.writerow((fullname, label, fname.split("-")[-1], st))


def get_folder(fname):
    if TARGETSPEAKER in fname:
        return ACPATH / "target"
    return ACPATH / "sources"


def extract(
    dfpath: str,
    outputpath: str,
    wavdir: str,
    nsplits: int = 6,
    test: bool = False,
    **dfkwargs,
):
    acparams.extractors.base._extract(
        listener, split_phones, dfpath, outputpath, wavdir, nsplits, test, **dfkwargs
    )
