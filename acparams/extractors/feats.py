import re
import csv
from typing import List

import numpy as np
import librosa
import python_speech_features
import pandas as pd

import acparams.textgrid
import acparams.audio
import acparams.extractors.base


NONPHONE = ["spn", "", "sil"]


def remove_nonwords(
    tgrid: acparams.textgrid.TextGrid, min_length: float = 0.5
) -> List[acparams.textgrid.Interval]:
    no_sil = []
    for word_intv in tgrid.words:
        is_long = word_intv.xmax - word_intv.xmin >= min_length
        no_special = re.match("\\[.*\\]", word_intv.name) is None
        if is_long and word_intv.name not in NONPHONE and no_special:
            no_sil.append(word_intv)
    return no_sil


def interval_chunks(intvs: List, tmax: float = 3):
    base = intvs[0].xmin
    chunk = [intvs[0]]
    larger_intervals = []
    for wi in intvs[1:]:
        if wi.xmax - base > tmax:
            large = acparams.textgrid.concat(chunk)
            larger_intervals.append(large)
            base = wi.xmax
            chunk = []
        chunk.append(wi)
    return larger_intervals


def mfcc_vec(audio: np.ndarray):
    mfccs = python_speech_features.mfcc(audio, 16000, appendEnergy=False)
    mean_mfccs = np.mean(mfccs[:, 1:], axis=0).reshape(-1)
    deltas = python_speech_features.delta(mfccs, 2)
    mean_deltas = np.mean(deltas[:, 1:], axis=0).reshape(-1)
    feats = np.concatenate((mean_mfccs, mean_deltas))
    return feats


def make_csv(df: pd.DataFrame, wavdir: str, queue):
    unaligned = 0
    for _, row in df.iterrows():
        tgrid = acparams.extractors.base.try_to_parse(row)
        if not tgrid:
            unaligned += 1
        else:
            wavpath = f"{wavdir}/{row['speaker']}/{row['fname']}.wav"
            wav, rate = librosa.load(wavpath, sr=None)  # type: ignore

            no_sil = remove_nonwords(tgrid)
            larger_intervals = interval_chunks(no_sil, 3)
            audio_intvs = acparams.audio.split(larger_intervals, wav, rate, offset=160)

            fnum = 0
            for word_data in audio_intvs:
                feats = mfcc_vec(word_data)
                queue.put(
                    tuple(feats) + tuple([f"{row['fname']}-{fnum}", row["state"]])
                )
                fnum += 1


def listener(q, outputpath):
    feats_csv = open(outputpath, "w", newline="")
    feats_writer = csv.writer(feats_csv, dialect="unix")
    cols = [f"mfcc{i+1}" for i in range(12)]
    delta_cols = [f"delta{i+1}" for i in range(12)]
    cols.extend(delta_cols)
    cols.extend(["fname", "state"])
    feats_writer.writerow(cols)
    while True:
        m = q.get()
        if m == "kill":
            feats_csv.close()
            break
        feats_writer.writerow(m)


def extract(
    dfpath: str,
    outputpath: str,
    wavdir: str,
    nsplits: int = 6,
    test: bool = False,
    **dfkwargs,
):
    acparams.extractors.base._extract(
        listener,
        make_csv,
        dfpath,
        outputpath,
        wavdir,
        nsplits=nsplits,
        test=test,
        **dfkwargs,
    )
