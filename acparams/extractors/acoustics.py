import csv

import librosa
import pandas as pd
import numpy as np

import acparams.audio
import acparams.phone
import acparams.textgrid
import acparams.extractors.base


VOWELS = ["ɛ", "e", "ĩ", "i", "ɔ", "o", "u", "ɐ", "a", "ẽ", "ɐ̃", "õ", "ũ"]
NONPHONE = ["spn", "", "sil"]
UNIVOWELS = [
    b"\\u025b",
    b"e",
    b"e\\u0303",
    b"i",
    b"i\\u0303",
    b"o",
    b"o\\u0303",
    b"\\u0254",
    b"u",
    b"u\\u0303",
    b"a",
    b"\\u0250",
    b"\\u0250\\u0303",
]
RHOTICS = ["ɾ", "ʁ"]
UNIRHOTICS = [
    b"\\u027e",
    b"\\u0281",
]


def unicode_phone(phon: str, unicodes: list) -> bool:
    return phon.encode("unicode_escape") in unicodes


# TO-DO: split into functions that read the wavs and that compute the acoustics
def compute_acoustics(df: pd.DataFrame, wavdir: str, queue):
    qtd_no_align = 0
    for _, row in df.iterrows():
        tgrid = acparams.extractors.base.try_to_parse(row)
        if not tgrid:
            qtd_no_align += 1
        else:
            wavpath = f"{wavdir}/{row['speaker']}/{row['fname']}.wav"
            wav, rate = librosa.load(wavpath, sr=None)  # type: ignore
            phones = acparams.audio.split(tgrid.phones, wav, rate)

            for p, intv in zip(phones, tgrid.phones):
                if unicode_phone(intv.name, UNIRHOTICS):
                    acoustics = acparams.phone.rhotic_analysis(
                        p.astype(np.float32), rate
                    )
                    phoninfo = (
                        intv.name,
                        row["state"],
                        row["gender"],
                        row["age"],
                        row["speaker"],
                        row["caption"],
                    )
                    queue.put(acoustics + phoninfo)


def listener(q, outputfpath):
    acoustics_csv = open(outputfpath, "w", newline="")
    acousticswriter = csv.writer(acoustics_csv, dialect="unix")
    acousticswriter.writerow(
        [
            # "meanf0",
            "length",
            # "meanDB",
            "f4f3",
            "f5f4",
            "phoneme",
            "state",
            "gender",
            "age",
            "speaker",
            "caption",
        ]
    )
    while True:
        m = q.get()
        if m == "kill":
            acoustics_csv.close()
            break
        acousticswriter.writerow(m)


def extract(
    dfpath: str,
    outputfpath: str,
    wavdir: str,
    nsplits: int = 6,
    test: bool = False,
    **df_kwargs,
):
    acparams.extractors.base._extract(
        listener,
        compute_acoustics,
        dfpath,
        outputfpath,
        wavdir,
        nsplits=nsplits,
        test=test,
        **df_kwargs,
    )
