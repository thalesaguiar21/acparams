import math
import pathlib
import multiprocessing as mp

import pandas as pd

import acparams.textgrid


TGRID_BASEPATH = pathlib.PosixPath("~/Dev/datasets/ttbacc/ptbr-aligned").expanduser()
DATAPATH = pathlib.PosixPath("~/Dev/datasets/ttbacc").expanduser()


def try_to_parse(row):
    tgridpath = f"{TGRID_BASEPATH}/{row['speaker']}/{row['fname']}.TextGrid"
    try:
        return acparams.textgrid.parse(tgridpath)
    except FileNotFoundError:
        return None


def split(df: pd.DataFrame, nsplits: int = 5):
    size = len(df.index)
    step = math.floor(size / nsplits)
    splits = []
    for i in range(nsplits):
        splits.append(df.iloc[i * step : (i + 1) * step])
    return splits


def _extract(
    listener,
    worker,
    dfpath: str,
    output_file: str,
    dir: str,
    nsplits: int = 6,
    test: bool = False,
    **df_kwargs,
):
    print("Splitting dataframe...")
    df = pd.DataFrame(pd.read_csv(dfpath, **df_kwargs))
    if test:
        df = df[:60]
    dfsplits = split(df, nsplits=nsplits)

    manager = mp.Manager()
    queue = manager.Queue()
    pool = mp.Pool(nsplits)
    print("Adding listener...")
    pool.apply_async(listener, (queue, output_file))  # type: ignore

    print("Adding jobs...")
    jobs = []
    for subset in dfsplits:
        job = pool.apply_async(worker, (subset, dir, queue))
        jobs.append(job)

    for job in jobs:
        job.get()

    queue.put("kill")
    pool.close()
    pool.join()
