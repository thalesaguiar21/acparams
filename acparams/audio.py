import numpy as np
import acparams.textgrid


def duration(signals: list, fs: int) -> list[float]:
    return [len(signal) / fs for signal in signals]


def split(
    intvs: list[acparams.textgrid.Interval], wav: np.ndarray, rate: int, offset: int = 0
):
    wavs = []
    for intv in intvs:
        sintv = acparams.textgrid.tosamples(intv, rate)
        sintv.xmin -= offset
        sintv.xmax += offset
        if sintv.xmin >= 0 and sintv.xmax < wav.size:
            wavs.append(np.copy(wav[np.arange(sintv.xmin, sintv.xmax)]))
    return wavs
