import numpy as np
import librosa
import math
from scipy import signal


def analysis(phone: np.ndarray, rate: int, nfft: int = 128, emph: float = 0.68):
    procphone = _preprocess(phone, emph)
    mean_f0 = np.nanmean(ffreq(phone, rate))
    length = duration(phone, rate)
    mean_dB = np.mean(intensity(procphone, nfft))
    fns = formants(procphone, rate)
    while len(fns) < 3:
        fns.append(np.nan)

    return mean_f0, length, mean_dB, fns[0], fns[1]


def rhotic_analysis(phone: np.ndarray, rate: int):
    length = duration(phone, rate)
    fmts = formants(phone, rate)
    fmt43_diff = np.nan if len(fmts) < 5 else fmts[4] - fmts[3]
    fmt54_diff = np.nan if len(fmts) < 6 else fmts[5] - fmts[4]
    return length, fmt43_diff, fmt54_diff


def _preprocess(phone: np.ndarray, emph: float):
    wnd_phone = phone * np.hamming(phone.size)
    emph_phone = signal.lfilter([1], [1, emph], wnd_phone)
    return emph_phone


def ffreq(phone: np.ndarray, rate: int, **kwargs):
    f0, *_ = librosa.pyin(
        y=phone, fmin=15, fmax=3500, sr=rate, frame_length=400, **kwargs
    )
    return f0


def duration(phone: np.ndarray, rate: int):
    return math.floor(phone.size / rate * 1000)


def intensity(phone: np.ndarray, nfft: int):
    magspec = np.abs(librosa.stft(phone, n_fft=nfft, window="hamming"))
    dbspec = librosa.amplitude_to_db(magspec, ref=np.max)  # type: ignore
    return dbspec


def formants(phone: np.ndarray, rate: int):
    fmts = []
    order = 2 + math.floor(rate / 1000)
    lpcs = librosa.lpc(phone, order=order)
    if not (np.isnan(lpcs).any() or np.isinf(lpcs).any()):
        roots = np.roots(lpcs)
        proots = roots[np.imag(roots) >= 0]
        angles = np.arctan2(np.imag(proots), np.real(proots))
        frqs = angles * (rate / (2 * np.pi))
        sind = np.argsort(frqs)
        frqs = frqs[sind]
        bandwidths = (rate / (-4 * np.pi)) * np.log(np.abs(proots[sind]))
        fmts = [fr for fr, bw in zip(frqs, bandwidths) if fr > 90 and bw < 400]
    return fmts
