import math
from dataclasses import dataclass
from typing import List, Set

import numpy as np
import matplotlib.pyplot as plt
import matplotlib


IPA2UNICODE = {
    b"\\u0250\\u0303": "/\\~a/",
    b"\\u0250": "/a/",
    b"\\u027e": "/r/",
    b"\\u0268": "-i",
    b"u\\u0303": "~u",
    b"e\\u0303": "e",
    b"\\u0283": "esh",
}


@dataclass
class Interval:
    xmin: float
    xmax: float
    name: str


def filt_by_label(intvs: list[Interval], lbl: str):
    return [i for i in intvs if i.name == lbl]


def _filt_by_time(intvs: list[Interval], tmin: float, tmax: float):
    intervals = []
    for i in intvs:
        if i.xmax > tmax:
            break
        elif i.xmin >= tmin:
            intervals.append(i)
    return intervals


def concat(intervals: list) -> Interval:
    intv_concat = Interval(
        xmin=intervals[0].xmin,
        xmax=intervals[-1].xmax,
        name=" ".join([intv.name for intv in intervals]),
    )
    return intv_concat


def intersect_by_label(inta: List[Interval], intb: List[Interval]) -> List[Interval]:
    """Elements of inta that are in intb regarding their names"""
    b_labels = unq_labels(intb)
    return [a for a in inta if a.name in b_labels]


def unq_labels(intvs: List[Interval]) -> Set[str]:
    return set([i.name for i in intvs])


class TextGrid:
    def __init__(self, words: list = None, phones: list = None) -> None:  # type: ignore
        self.words = words if words else []
        self.phones = phones if phones else []

    def phones_labeled(self, lbl: str):
        return filt_by_label(self.phones, lbl)

    def words_labeled(self, lbl: str):
        return filt_by_label(self.words, lbl)

    def in_interval(self, tmin: float, tmax: float) -> tuple:
        phones = _filt_by_time(self.phones, tmin, tmax)
        words = _filt_by_time(self.words, tmin, tmax)
        return (phones, words)


def subgrid(tgrid: TextGrid, tmin: float, tmax: float):
    phones, words = tgrid.in_interval(tmin, tmax)
    return TextGrid(phones, words)


def parse(fpath: str) -> TextGrid:
    tgrid = TextGrid()
    with open(fpath, "r") as gridfile:
        word_lines = []
        line = gridfile.readline()
        while line and line.strip() != 'name = "phones"':
            word_lines.append(line)
            line = gridfile.readline()
        tgrid.words = _lines2intervals(word_lines[14:-2])
        phone_lines = gridfile.read().splitlines()[3:]
        tgrid.phones = _lines2intervals(phone_lines)
    return tgrid


def _lines2intervals(lines: list[str]) -> list:
    intervals = []
    for i in range(0, len(lines), 4):
        interval = Interval(0, 0, "")
        interval.xmin = float(lines[i + 1].split("=")[1].strip())
        interval.xmax = float(lines[i + 2].split("=")[1].strip())
        interval.name = lines[i + 3].split("=")[1].strip().replace('"', "")
        intervals.append(interval)
    return intervals


def tosamples(intv: Interval, rate: int) -> Interval:
    xmin_s = math.floor(intv.xmin * rate)
    xmax_s = math.floor(intv.xmax * rate)
    return Interval(xmin_s, xmax_s, intv.name)


def plot(
    fpath: str, tgrid: TextGrid, wav: np.ndarray, rate: int, xmin: float, xmax: float
):
    """Creates a plot of the spectrogram and the labels for phonemes and words
    at the given time interval.

    Args:
        fpath: path to save the figure
        tgrid: the audio TextGrid
        wav: the audio in waveform
        rate: audio sampling rate
        xmin: starting section of the audio
        xmax: end section of the audio
    """
    with matplotlib.rc_context({"figure.figsize": (20, 6), "font.size": 20}):
        wavcut = wav[math.floor(xmin * rate) : math.floor(xmax * rate)]
        _, freqs, _, _ = plt.specgram(
            wavcut, NFFT=128, noverlap=98, Fs=rate, cmap="viridis"
        )
        for phone in tgrid.phones:
            if phone.xmax >= xmax:
                break
            elif phone.xmin >= xmin:
                dmax = phone.xmax - xmin
                dmin = phone.xmin - xmin
                plt.vlines(
                    x=dmax,
                    ymin=0,
                    ymax=freqs.max(),
                    colors="red",
                    linewidths=0.7,
                )
                plt.text(
                    dmin + (dmax - dmin) / 2,
                    8200,
                    IPA2UNICODE.get(phone.name.encode("unicode_escape"), phone.name),
                    ha="center",
                    fontsize="large",
                )
        for word in tgrid.words:
            if word.xmax >= xmax:
                break
            elif word.xmin >= xmin:
                dmax = word.xmax - xmin
                dmin = word.xmin - xmin
                plt.text(
                    dmin + (dmax - dmin) / 2,
                    8800,
                    word.name.upper(),
                    ha="center",
                    fontsize="large",
                    fontstyle="italic",
                )
        plt.ylabel(r"1000 (Hz)", fontsize=10)
        plt.yticks([0, 2000, 4000, 6000, 8000], labels=[0, 2, 4, 6, 8])
        # plt.xlabel("Time (s)")
        plt.tight_layout()
        plt.savefig(fpath, dpi=300)
        plt.close()
