from . import context
from acparams import phone

import unittest
from scipy.io import wavfile
import numpy as np


class AGuPhoneme(unittest.TestCase):
    def test_should_have(self):
        gupath = "./tests/files/gu.wav"
        rate, wav = wavfile.read(gupath)
        f0, length, intensity, fns = phone.analysis(wav.astype(np.float32), rate)
        self.assertEqual(intensity.size, 1)
        self.assertEqual(length, 688)
        self.assertEqual(f0.size, 1)
        self.assertEqual(len(fns), 3)
        self.assertEqual(fns[0] < 400 and fns[0] > 0, True)
        self.assertEqual(fns[1] < 5800 and fns[1] > 4200, True)
        self.assertEqual(fns[2] < 7200 and fns[2] > 6200, True)
