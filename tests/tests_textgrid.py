from . import context
from acparams import textgrid

from scipy.io import wavfile
import math
import unittest


class AnEmptyFileTests(unittest.TestCase):
    def test_is_empty(self):
        path = "./tests/files/empty-grid.TextGrid"
        tgrid = textgrid.parse(path)
        self.assertEqual(tgrid.phones, [], "there are phones")
        self.assertEqual(tgrid.words, [], "there are words")

    def test_has_wrong_format(self):
        path = "./tests/files/wrong-grid-fmt.TextGrid"
        tgrid = textgrid.parse(path)
        self.assertEqual(tgrid.phones, [], "there are phones")
        self.assertEqual(tgrid.words, [], "there are words")


class MissingInfoTests(unittest.TestCase):
    def test_raise_value_error(self):
        filepaths = [
            "./tests/files/empty-xmin.TextGrid",
            "./tests/files/empty-xmax.TextGrid",
        ]
        for path in filepaths:
            with self.assertRaises(ValueError, msg=f"for {path}"):
                textgrid.parse(path)

    def test_keeps_empty_name(self):
        path = "./tests/files/empty-name.TextGrid"
        tgrid = textgrid.parse(path)
        self.assertEqual(tgrid.phones[0].xmin, 0)
        self.assertEqual(tgrid.phones[0].xmax, 0.23)
        self.assertEqual(tgrid.phones[0].name, "")


class PlottingTests(unittest.TestCase):
    def test_when_expected(self):
        tgridpath = "./tests/files/ted-talk-0312.TextGrid"
        tgrid = textgrid.parse(tgridpath)
        wavpath = "./tests/files/ted-talk-0312.wav"
        rate, wav = wavfile.read(wavpath)
        textgrid.plot(
            "./tests/files/ted-talk-0312-view",
            tgrid,
            wav[: math.floor(4.2 * rate)],
            rate,
        )


class WithoutPhonesTests(unittest.TestCase):
    def setUp(self) -> None:
        self.tgrid = textgrid.TextGrid(
            words=[textgrid.Interval(0.1, 0.2, "w"), textgrid.Interval(0.3, 0.4, "r")]
        )

    def test_has_empty_list(self) -> None:
        self.assertEqual(self.tgrid.phones, [])

    def test_filt_returns_empty(self) -> None:
        self.assertEqual(self.tgrid.phones_labeled("a"), [])
        self.assertEqual(self.tgrid.phones_labeled(""), [])
        self.assertEqual(self.tgrid.phones_labeled(" "), [])

    def test_filt_by_time_has_no_phones(self) -> None:
        phones, words = self.tgrid.in_interval(0.1, 0.2)
        self.assertEqual(phones, [])
        self.assertEqual(words, [textgrid.Interval(0.1, 0.2, "w")])


class WithoutWordsTests(unittest.TestCase):
    def setUp(self) -> None:
        self.tgrid = textgrid.TextGrid(
            phones=[textgrid.Interval(0.1, 0.2, "w"), textgrid.Interval(0.3, 0.4, "r")]
        )

    def test_has_empty_list(self) -> None:
        self.assertEqual(self.tgrid.words, [])

    def test_filt_return_empty_list(self) -> None:
        self.assertEqual(self.tgrid.words_labeled("awords"), [])
        self.assertEqual(self.tgrid.words_labeled(""), [])
        self.assertEqual(self.tgrid.words_labeled(" "), [])
        self.assertEqual(self.tgrid.words_labeled(","), [])

    def test_filt_by_time_has_no_words(self) -> None:
        phones, words = self.tgrid.in_interval(0.1, 0.2)
        self.assertEqual(phones, [textgrid.Interval(0.1, 0.2, "w")])
        self.assertEqual(words, [])


class CorrectTGridTests(unittest.TestCase):
    def setUp(self) -> None:
        self.tgrid = textgrid.TextGrid(
            words=[
                textgrid.Interval(0.1, 0.2, "wordA"),
                textgrid.Interval(0.3, 0.4, "wordR"),
            ],
            phones=[
                textgrid.Interval(0.1, 0.2, "pa"),
                textgrid.Interval(0.3, 0.4, "pb"),
                textgrid.Interval(0.5, 0.74, "pb"),
            ],
        )

    def test_has_three_phns(self) -> None:
        self.assertEqual(len(self.tgrid.phones), 3)

    def test_has_two_words(self) -> None:
        self.assertEqual(len(self.tgrid.words), 2)

    def test_filt_phn_is_nonempty(self) -> None:
        filtered = self.tgrid.phones_labeled("pb")
        self.assertEqual(len(filtered), 2)
        self.assertEqual(filtered[0].name, "pb")
        self.assertEqual(filtered[1].name, "pb")

    def test_filt_words_is_nonempty(self) -> None:
        filtered = self.tgrid.words_labeled("wordA")
        self.assertEqual(len(filtered), 1)
        self.assertEqual(filtered[0].name, "wordA")

    def test_filt_by_time_returns_all(self) -> None:
        for tmin, tmax in [(0.0, 0.75), (-1, 0.75), (0.1, 0.75), (0.1, 1.25)]:
            with self.subTest(i=(tmin, tmax)):
                phones, words = self.tgrid.in_interval(tmin, tmax)
                self.assertEqual(len(phones), 3)
                self.assertEqual(len(words), 2)

    def test_filt_by_time_returns_empty_intervals(self) -> None:
        for tmin, tmax in [(3.5, 4.5), (4.5, 3.5), (0.0, 0.0), (0.1001, 0.1999)]:
            with self.subTest(i=(tmin, tmax)):
                phones, words = self.tgrid.in_interval(tmin, tmax)
                self.assertEqual(phones, [], "has phone")
                self.assertEqual(words, [], "has word")

    def test_filt_by_time_returns_wordR(self) -> None:
        phones, words = self.tgrid.in_interval(0.3, 0.4)
        self.assertEqual(phones[0], textgrid.Interval(0.3, 0.4, "pb"))
        self.assertEqual(words[0], textgrid.Interval(0.3, 0.4, "wordR"))
