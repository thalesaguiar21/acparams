from . import context
from acparams import plots

import unittest

from scipy.io import wavfile


class SpectrogramTests(unittest.TestCase):

    @unittest.skip("TODO")
    def test_simple_spec(self):
        fs, phoneme = wavfile.read('audio/gu.wav')
        plots.multi_spectrogram(phoneme, 'example', fs=fs)
