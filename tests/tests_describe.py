from . import context
from acparams import params

import unittest
import numpy as np


class DescribeTests(unittest.TestCase):

    @unittest.skip("TODO")
    def test_positive(self):
        legal_data = [[1, 2, 3, 4, 5],
                      np.arange(1, 6),
                      [1, 2.5],
                      [1] * 10,
                      [-1, -2, 1, 2]]
        expected = [(3, 1.4142135623730951, 1, 1),
                    (3, 1.4142135623730951, 1.0, 1),
                    (1.75, 0.75, 1, 1),
                    (1, 0, 1, 10),
                    (0, 1.5811388300841898, -2, 1)]

        for ldata, expec in zip(legal_data, expected):
            with self.subTest():
                descr = params.describe(ldata)
                self.assertEqual(expec, descr)
