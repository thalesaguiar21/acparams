from . import context
from acparams import audio
from acparams import textgrid

import unittest
from scipy.io import wavfile

AUDIOPATH = "./tests/files/ted-talk-0312.wav"
GRIDPATH = "./tests/files/ted-talk-0312.TextGrid"
PARTGRIDPATH = "./tests/files/ted-talk-0312-part.TextGrid"


class DurationTests(unittest.TestCase):
    @unittest.skip("TODO")
    def test_ndurations(self):
        durations = audio.duration([], 16000)
        self.assertEqual(50, len(durations))


class SplitPhonesTests(unittest.TestCase):
    def setUp(self) -> None:
        self.rate, self.wav = wavfile.read(AUDIOPATH)
        self.tgrid = textgrid.parse(GRIDPATH)

    def test_has_full_length_grid(self):
        phones = audio.split(self.tgrid.phones, self.wav, self.rate)
        self.assertEqual(phones.shape[0], 4736)


class SplitWithPartialGrid(unittest.TestCase):
    def setUp(self) -> None:
        self.rate, self.wav = wavfile.read(AUDIOPATH)
        self.tgrid = textgrid.parse(PARTGRIDPATH)

    def test_must_have_same_intervals(self):
        phones = audio.split(self.tgrid.phones, self.wav, self.rate)
        self.assertEqual(phones.shape[0], 4726)

    def test_has_grid_without_phones(self):
        pass
